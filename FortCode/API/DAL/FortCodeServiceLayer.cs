﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.API.ApiModels;
using FortCode.Data2;
using FortCode.ViewModels;

namespace FortCode.API.DAL
{
    public class FortCodeServiceDal : FortCodeInterface
    {

        public List<FavoriteCityViewModel> GetFavoriteCities(AppUser appUser)
        {
            List<FavoriteCityViewModel> retList = new List<FavoriteCityViewModel>();

            using (var db = new FortCodeDbContext())
            {
                var query = from u in db.AppUsers.Where(s => s.UserName == appUser.UserName)
                            join bm in db.UserMapSecretBars on u.AspNetUserId equals bm.UserId
                            join b in db.Bars on bm.BarId equals b.BarId
                            join c in db.Cities on b.CityId equals c.CityId
                            select new FavoriteCityViewModel { UserName = u.UserName, UserId = u.UserId, CityId = c.CityId, CityName = c.CityName, CountryName = c.CountryName, BarId = b.BarId, BarName = b.BarName };

                retList = query.OrderBy(s => s.CityName).ToList();

            }

            return retList;
        }

        public bool AddFavoriteCity(AppUser appUser, City city)
        {
            bool retVal = false;

            List<FavoriteCityViewModel> retList = this.GetFavoriteCities(appUser);

            var cityExists = retList.Select(c => c.CityName).Contains(city.CityName);
            if (!cityExists)
            {
                try
                {
                    using (var db = new FortCodeDbContext())
                    {
                        UserMapCity userMapCity = new UserMapCity();
                        userMapCity.UserMapCityId = Guid.NewGuid();
                        userMapCity.UserId = appUser.UserId;
                        userMapCity.CityId = city.CityId;

                        db.UserMapCitys.Add(userMapCity);

                        db.SaveChanges();
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return retVal;
        }

        public bool RemoveFavoriteCity(AppUser appUser, City city)
        {
            try
            {
                using (var db = new FortCodeDbContext())
                {

                    var cityMap = db.UserMapCitys.Where(s => s.UserId == appUser.UserId && s.CityId == city.CityId).FirstOrDefault();

                    if (cityMap != null)
                    {
                        db.UserMapCitys.Remove(cityMap);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AddFavoriteBar(AppUser appUser, SecretBar bar)
        {
            bool retVal = false;

            List<FavoriteCityViewModel> retList = this.GetFavoriteCities(appUser);

            var barExists = retList.Select(c => c.BarName).Contains(bar.BarName);
            if (!barExists)
            {
                try
                {
                    using (var db = new FortCodeDbContext())
                    {
                        UserMapSecretBar userMapSecretBar = new UserMapSecretBar();
                        userMapSecretBar.UserMapSecretBarId = Guid.NewGuid();
                        userMapSecretBar.UserId = appUser.UserId;
                        userMapSecretBar.BarId = bar.BarId;

                        db.UserMapSecretBars.Add(userMapSecretBar);

                        db.SaveChanges();
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }

            return retVal;
        }

        public bool RemoveFavoriteBar(AppUser appUser, SecretBar bar)
        {
            try
            {
                using (var db = new FortCodeDbContext())
                {

                    var userMapBar = db.UserMapSecretBars.Where(s => s.UserId == appUser.UserId && s.BarId == bar.BarId).FirstOrDefault();

                    if (userMapBar != null)
                    {
                        db.UserMapSecretBars.Remove(userMapBar);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<FavoriteCityViewModel> GetFavoriteBarsByCityName(AppUser appUser, string cityName)
        {
            List<FavoriteCityViewModel> retList = new List<FavoriteCityViewModel>();

            using (var db = new FortCodeDbContext())
            {
                var query = from u in db.AppUsers.Where(s => s.UserName == appUser.UserName)
                            join bm in db.UserMapSecretBars on u.AspNetUserId equals bm.UserId
                            join b in db.Bars on bm.BarId equals b.BarId
                            join c in db.Cities.Where(s => s.CityName == cityName) on b.CityId equals c.CityId
                            select new FavoriteCityViewModel { UserName = u.UserName, UserId = u.UserId, CityId = c.CityId, CityName = c.CityName, CountryName = c.CountryName, BarId = b.BarId, BarName = b.BarName };

                retList = query.OrderBy(s => s.CityName).ToList();

            }

            return retList;
        }

        public List<UserSharedBarViewModel> FindSharedBars(AppUser appUser)
        {
            List<UserSharedBarViewModel> retList = new List<UserSharedBarViewModel>();
            List<FavoriteCityViewModel> commonFavorites = new List<FavoriteCityViewModel>();

            using (var db = new FortCodeDbContext())
            {
                var queryAllFavorites = from u in db.AppUsers
                                        join bm in db.UserMapSecretBars on u.AspNetUserId equals bm.UserId
                                        join b in db.Bars on bm.BarId equals b.BarId
                                        join c in db.Cities on b.CityId equals c.CityId
                                        select new FavoriteCityViewModel { UserName = u.UserName, UserId = u.UserId, CityId = c.CityId, CityName = c.CityName, CountryName = c.CountryName, BarId = b.BarId, BarName = b.BarName };


                var queryMyFavorites = from u in db.AppUsers.Where(s => s.UserName == appUser.UserName)
                                       join bm in db.UserMapSecretBars on u.AspNetUserId equals bm.UserId
                                       join b in db.Bars on bm.BarId equals b.BarId
                                       join c in db.Cities on b.CityId equals c.CityId
                                       select new FavoriteCityViewModel { UserName = u.UserName, UserId = u.UserId, CityId = c.CityId, CityName = c.CityName, CountryName = c.CountryName, BarId = b.BarId, BarName = b.BarName };


                var allFavorites = queryAllFavorites.ToList();

                var myFavorites = queryMyFavorites.ToList();

                foreach( var item in allFavorites)
                {
                    var testItem = myFavorites.Any(t1 => t1.BarName == item.BarName);

                    if(testItem)
                    {
                        commonFavorites.Add(item);
                    }
                }


                var query3 = from group1 in commonFavorites.ToList()
                             group group1 by group1.BarName into newGroup
                             orderby newGroup.Key
                             select new UserSharedBarViewModel { SharedBarName = newGroup.Key, SharedCount = newGroup.Count() };

                retList = query3.ToList();
            }

            return retList;
        }

        public List<AppUser> GetAllAppUsers()
        {
            List<AppUser> retList = new List<AppUser>();


            return retList;
        }
    }

    
}
