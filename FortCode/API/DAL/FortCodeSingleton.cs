﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.API.DAL;

namespace FortCode.API.DAL
{
    public class FortCodeSingleton
    {
        private static FortCodeServiceDal _fortCodeService;
        public static FortCodeServiceDal FortCodeService
        {
            get
            {
                if (_fortCodeService == null)
                {
                    _fortCodeService = new FortCodeServiceDal();
                }

                return _fortCodeService;
            }
        }

    }
}
