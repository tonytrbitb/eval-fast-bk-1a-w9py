﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.API.ApiModels;
using FortCode.ViewModels;

namespace FortCode.API
{
    public interface FortCodeInterface
    {
        //bool AuthenticAppUser(AppUser appUser);

        List<FavoriteCityViewModel> GetFavoriteCities(AppUser appUser);

        bool AddFavoriteCity(AppUser appUser, City city);

        bool RemoveFavoriteCity(AppUser appUser, City city);

        List<FavoriteCityViewModel> GetFavoriteBarsByCityName(AppUser appUser, string cityName);

        bool AddFavoriteBar(AppUser appUser, SecretBar bar);

        bool RemoveFavoriteBar(AppUser appUser, SecretBar bar);

        List<UserSharedBarViewModel> FindSharedBars(AppUser appUser);

        List<AppUser> GetAllAppUsers();

        //bool AddFavoriteDrink(AppUser appUser, Drink drink);

        //bool RemoveFavoriteDrink(AppUser appUser, Drink drink);

    }
}
