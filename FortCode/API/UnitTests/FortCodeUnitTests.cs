﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;
using FortCode.API.ApiModels;
using FortCode.Data2;
using FortCode.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace FortCode.API.UnitTests
{
    public class FortCodeUnitTests
    {
        private FortCodeDbContext _context;

        public FortCodeUnitTests(FortCodeDbContext context)
        {
            _context = context;
        }

        [SetUp]
        public void Setup()
        {


        }

        [Test]
        public void TestGetFavoriteCitiesMoq()
        {
            var appUser1 = new FortCode.API.ApiModels.AppUser();
            appUser1.UserId = new Guid("00000000-0000-0000-0000-000000000000");
            appUser1.UserName = "AppUser1";
            appUser1.UserPassword = "Password1";

            var appUser2 = new FortCode.API.ApiModels.AppUser();
            appUser2.UserId = new Guid("11111111-0000-0000-0000-000000000000");
            appUser2.UserName = "AppUser2";
            appUser2.UserPassword = "Password2";

            var appUser3 = new FortCode.API.ApiModels.AppUser();
            appUser3.UserId = new Guid("22222222-0000-0000-0000-000000000000");
            appUser3.UserName = "AppUser3";
            appUser3.UserPassword = "Password3";

            var usersList = new List<AppUser>();
            usersList.Add(appUser1);
            usersList.Add(appUser2);
            usersList.Add(appUser3);

            var mockSet = new Mock<DbSet<AppUser>>();
            mockSet.As<IQueryable<AppUser>>().Setup(m => m.Provider).Returns(usersList.AsQueryable().Provider);
            mockSet.As<IQueryable<AppUser>>().Setup(m => m.Expression).Returns(usersList.AsQueryable().Expression);
            mockSet.As<IQueryable<AppUser>>().Setup(m => m.ElementType).Returns(usersList.AsQueryable().ElementType);
            mockSet.As<IQueryable<AppUser>>().Setup(m => m.GetEnumerator()).Returns(usersList.AsQueryable().GetEnumerator());

            var mockContext = new Mock<FortCodeDbContext>();
            mockContext.Setup(c => c.AppUsers).Returns(mockSet.Object);

            var service = new FortCodeUnitTests(mockContext.Object);

            var retAppUserList = service.GetAllAppUsers();

        }

        public List<AppUser> GetAllAppUsers()
        {
            var query = from b in _context.AppUsers
                        orderby b.UserName
                        select b;

            return query.ToList();
        }
    }
}
