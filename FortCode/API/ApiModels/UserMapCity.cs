﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.API.ApiModels
{

    [Table("UserMapCity")]
    public class UserMapCity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserMapCity()
        {

        }

        [Key]
        public Guid UserMapCityId { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid CityId { get; set; }

    }
}
