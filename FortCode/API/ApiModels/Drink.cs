﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.API.ApiModels
{
    [Table("Drink")]
    public class Drink
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Drink()
        {


        }

        [Key]
        public Guid DrinkId { get; set; }


        [Required]
        [StringLength(127)]
        public string DrinkName { get; set; }

    }
}
