﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.API.ApiModels
{
    [Table("City")]
    public partial class City
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public City()
        {

        }

        [Key]
        public Guid CityId { get; set; }

        [Required]
        [StringLength(127)]
        public string CityName { get; set; }

        [Required]
        [StringLength(127)]
        public string CountryName { get; set; }

    }
}
