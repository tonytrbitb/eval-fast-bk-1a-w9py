﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.API.ApiModels
{
    [Table("UserMapBarMapDrink")]
    public class UserMapBarMapDrink
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserMapBarMapDrink()
        {

        }

        [Key]
        public Guid UserMapBarMapDrinkId { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid BarId { get; set; }

        [Required]
        public Guid DrinkId { get; set; }

    }
}
