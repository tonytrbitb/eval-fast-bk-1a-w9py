﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.API.ApiModels
{

    [Table("UserMapSecretBar")]
    public class UserMapSecretBar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserMapSecretBar()
        {

        }

        [Key]
        public Guid UserMapSecretBarId { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid BarId { get; set; }

    }
}
