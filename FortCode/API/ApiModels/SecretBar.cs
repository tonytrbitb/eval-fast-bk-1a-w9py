﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace FortCode.API.ApiModels
{
    [Table("SecretBar")]
    public partial class SecretBar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SecretBar()
        {

        }

        [Key]
        public Guid BarId { get; set; }

        [Required]
        [StringLength(127)]
        public string BarName { get; set; }

        [Required]
        public Guid CityId { get; set; }

    }
}
