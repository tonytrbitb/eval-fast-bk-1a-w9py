﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FortCode.API.ApiModels
{
    [Table("AppUser")]
    public class AppUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppUser()
        {
            FavoriteCities = new HashSet<City>();
            FavoriteBars = new HashSet<SecretBar>();
        }

        [Key]
        public Guid UserId { get; set; }


        [Required]
        [StringLength(127)]
        public string UserName { get; set; }

        [Required]
        [StringLength(15)]
        public string UserPassword { get; set; }


        public Guid? AspNetUserId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<City> FavoriteCities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SecretBar> FavoriteBars { get; set; }
    }
}
