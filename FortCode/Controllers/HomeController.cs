﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;


namespace FortCode.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {



            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}

        //public ActionResult GetImage(string fileName)
        //{
        //    if (!string.IsNullOrEmpty(fileName))
        //    {
        //        var importedCoinFromDb = DBContext.GetCoinImportByFileName(fileName);

        //        return File(importedCoinFromDb.ImageData, "image/*");
        //    }

        //    return File("0634_Roman_1_Silver_TetraDrachmn-Horned_1000_Obverse.png", "image/*");
        //}
    }

    
}
