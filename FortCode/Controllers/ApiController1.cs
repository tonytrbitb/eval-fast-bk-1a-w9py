﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FortCode.API.ApiModels;
using FortCode.Data2;
using FortCode.ViewModels;
using FortCode.API.DAL;

namespace FortCode.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ApiController1 : ControllerBase
    {
        [Route("/GetFavoriteCities")]
        [HttpPost]
        public List<FavoriteCityViewModel> GetFavoriteCities(AppUser appUser)
        {
            List<FavoriteCityViewModel> retList = new List<FavoriteCityViewModel>();
            retList = FortCodeSingleton.FortCodeService.GetFavoriteCities(appUser);
            return retList;
        }

        [Route("/GetFavoriteBarsByCityName")]
        [HttpPost]
        public List<FavoriteCityViewModel> GetFavoriteBarsByCityName(AppUser appUser, string cityName)
        {
            List<FavoriteCityViewModel> retList = new List<FavoriteCityViewModel>();
            retList = FortCodeSingleton.FortCodeService.GetFavoriteBarsByCityName(appUser, cityName);
            return retList;
        }


        //[Route("/AddFavoriteCity")]
        //[HttpPost]
        //public bool AddFavoriteCity(AppUser appUser, City city)
        //{
        //    bool retVal = false;
        //    retVal = FortCodeSingleton.FortCodeService.AddFavoriteCity(appUser, city);
        //    return retVal;
        //}

        //[Route("/RemoveFavoriteCity")]
        //[HttpPost]
        //public bool RemoveFavoriteCity(AppUser appUser, City city)
        //{
        //    bool retVal = false;
        //    retVal = FortCodeSingleton.FortCodeService.RemoveFavoriteCity(appUser, city);
        //    return retVal;
        //}

        //[Route("/AddFavoriteBar")]
        //[HttpPost]
        //public bool AddFavoriteBar(AppUser appUser, SecretBar bar)
        //{
        //    bool retVal = false;
        //    retVal = FortCodeSingleton.FortCodeService.AddFavoriteBar(appUser, bar);
        //    return retVal;
        //}

        //[Route("/RemoveFavoriteBar")]
        //[HttpPost]
        //public bool RemoveFavoriteBar(AppUser appUser, SecretBar bar)
        //{
        //    bool retVal = false;
        //    retVal = FortCodeSingleton.FortCodeService.RemoveFavoriteBar(appUser, bar);
        //    return retVal;
        //}

        [Route("/FindSharedBars")]
        [HttpPost]
        public List<UserSharedBarViewModel> FindSharedBars(AppUser appUser)
        {
            List<UserSharedBarViewModel> retList = new List<UserSharedBarViewModel>();
            retList = FortCodeSingleton.FortCodeService.FindSharedBars(appUser);
            return retList;
        }
    }

    
}
