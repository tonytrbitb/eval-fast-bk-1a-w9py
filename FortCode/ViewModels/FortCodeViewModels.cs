﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FortCode.ViewModels
{
    public class UserSharedBarViewModel
    {
        public Guid SharedBarId;

        public string SharedBarName { get; set; }

        public int SharedCount { get; set; }
    }

    public class CityViewModel
    {
        public Guid CityId { get; set; }
        public string CityName { get; set; }

        public string CountryName { get; set; }

        //public List<SecretBarViewModel> BarList { get; set; }
    }

    public class SecretBarViewModel
    {
        public Guid BarId { get; set; }

        public string BarName { get; set; }

        //public string DrinkName { get; set; }
    }

    public class DrinkViewModel
    {
        public Guid DrinkId { get; set; }

        public string DrinkName { get; set; }
    }

    public class UserViewModel
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public string UserPassword { get; set; }

        public List<CityViewModel> FavoriteCities { get; set; }



    }

    public class FavoriteCityViewModel
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public Guid CityId { get; set; }
        public string CityName { get; set; }

        public string CountryName { get; set; }

        public Guid BarId { get; set; }

        public string BarName { get; set; }

    }

}
