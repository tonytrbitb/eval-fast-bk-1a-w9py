﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;


namespace FortCode.Data2
{
    public partial class CreateFortCodeSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppUser",
                columns: table => new
                {
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(127)", nullable: false),
                    UserPassword = table.Column<string>(type: "nvarchar(15)", nullable: false),
                    AspNetUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppUser", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    CityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CityName = table.Column<string>(type: "nvarchar(127)", nullable: false),
                    CountryName = table.Column<string>(type: "nvarchar(127)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.CityId);
                });

            migrationBuilder.CreateTable(
                name: "SecretBar",
                columns: table => new
                {
                    BarId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BarName = table.Column<string>(type: "nvarchar(127)", nullable: false),
                    CityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecretBar", x => x.BarId);
                });

            migrationBuilder.CreateTable(
                name: "Drink",
                columns: table => new
                {
                    DrinkId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DrinkName = table.Column<string>(type: "nvarchar(127)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drink", x => x.DrinkId);
                });

            migrationBuilder.CreateTable(
               name: "UserMapSecretBar",
               columns: table => new
               {
                   UserMapSecretBarId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                   UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                   BarId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_UserMapSecretBar", x => x.UserMapSecretBarId);
               });

            migrationBuilder.CreateTable(
               name: "UserMapBarMapDrink",
               columns: table => new
               {
                   UserMapBarMapDrinkId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                   UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                   BarId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                   DrinkId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_UserMapBarMapDrink", x => x.UserMapBarMapDrinkId);
               });

            migrationBuilder.CreateTable(
               name: "UserMapCity",
               columns: table => new
               {
                   UserMapCityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                   UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                   CityId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_UserMapCity", x => x.UserMapCityId);
               });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppUser");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "SecretBar");

            migrationBuilder.DropTable(
                name: "Drink");

            migrationBuilder.DropTable(
                name: "UserMapSecretBar");

            migrationBuilder.DropTable(
                name: "UserMapBarMapDrink");

            migrationBuilder.DropTable(
               name: "UserMapCity");
        }
    }
}