﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace FortCode.Data2
{
    [DbContext(typeof(FortCodeDbContext))]
    [Migration("CreateFortCodeSchema")]
    partial class CreateFortCodeSchema
    {

        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.9");
                

            modelBuilder.Entity("FortCode.API.ApiModels.AppUser", b =>
            {
                b.Property<Guid>("UserId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("UserName")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("nvarchar(127)");

                b.Property<Guid>("UserPassword")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("nvarchar(15)");

                b.Property<Guid>("AspNetUserId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.ToTable("AppUser");
            });

            modelBuilder.Entity("FortCode.API.ApiModels.City", b =>
            {
                b.Property<Guid>("CityId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("CityName")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("nvarchar(127)");

                b.Property<Guid>("CountryName")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("nvarchar(127)");

                b.ToTable("City");
            });

            modelBuilder.Entity("FortCode.API.ApiModels.SecretBar", b =>
            {
                b.Property<Guid>("BarId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("BarName")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("nvarchar(127)");

                b.ToTable("SecretBar");
            });

            modelBuilder.Entity("FortCode.API.ApiModels.Drink", b =>
            {
                b.Property<Guid>("DrinkId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("DrinkName")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("nvarchar(127)");

                b.ToTable("Drink");
            });

            modelBuilder.Entity("FortCode.API.ApiModels.UserMapSecretBar", b =>
            {
                b.Property<Guid>("UserMapSecretBarId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("UserId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("BarId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.ToTable("UserMapSecretBar");
            });

            modelBuilder.Entity("FortCode.API.ApiModels.UserMapBarMapDrink", b =>
            {
                b.Property<Guid>("UserMapBarMapDrinkId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("UserId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("BarId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("DrinkId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.ToTable("UserMapBarMapDrink");
            });

            modelBuilder.Entity("FortCode.API.ApiModels.UserMapCity", b =>
            {
                b.Property<Guid>("UserMapCityId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("UserId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.Property<Guid>("CityId")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("uniqueidentifier");

                b.ToTable("UserMapCity");
            });


#pragma warning restore 612, 618
        }
    }
}
