using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Configuration;
using FortCode.API.ApiModels;

namespace FortCode.Data2
{
    public partial class FortCodeDbContext : DbContext
    {
        public static DbContextOptions getOptions()
        {
            DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder();
            var options = optionsBuilder.UseSqlServer("Name=DefaultConnection");
            return options.Options;
        }

        public static ConnectionStringSettings GetConnString()
        {
            var myConn = new ConnectionStringSettings("DefaultConnection", @"Server=(localdb)\mssqllocaldb;Database=Fort3;Trusted_Connection=True;MultipleActiveResultSets=true");

            return myConn;
        }

        public FortCodeDbContext() : base()
        {

        }

        public FortCodeDbContext(DbContextOptions options) : base(getOptions())
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(GetConnString().ConnectionString);
            }
        }

        public virtual DbSet<AppUser> AppUsers { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<SecretBar> Bars { get; set; }
        public virtual DbSet<Drink> Drinks { get; set; }

        public virtual DbSet<UserMapSecretBar> UserMapSecretBars { get; set; }

        public virtual DbSet<UserMapBarMapDrink> UserMapBarMapDrinks { get; set; }

        public virtual DbSet<UserMapCity> UserMapCitys { get; set; }
    }
}
