Fort Code Exercise2 Explanations 01/11/22

(1) I don't know how to use "Docker" so did the exercise using technologies and techniques that I am familiar with hoping to have enough time to implement the API I've created using "Docker technology after I finshed the basic API code.
(2) Although I wanted to implement the "Docker" portion of the exercise I don't believe I will have time to do it (I have Docker Desktop installed but I haven't used it yet and I prefer to learn it on my local setup on a really simple project first, I'm sure I'll get around to it sometime this week but for now I need to move on to a couple of other projects).

(3) These are features I have created for this exercise.

(3A)  I created a EF Data Model (Code First) for the "ApplicationDbContext" to allow the user to Register, Login and Authenticate.

(3B) I created a second EF Data Model (Code First) "FortCodeDbContext" for the API.

(3C) I used EF Code First to Generate the SQL Database using both DbContexts.

(3D) I added enough UI code so that the Web App allows the User to Register and Login.

(3E) I created an API with Interface, API Service and API Service Singleton.

(3F) I created a "appsetting.json" file that the DbContexts use.

(3G) I created a NUnit Test Class with Moq and added a simple example of using Moq to mock the DbContext.

(3H) I generated a SQL script of the database Schema and included it in the project.


