The API implments these methds:

        //bool AuthenticAppUser(AppUser appUser); // I'm going to finish this one before I move on to other projects.

        List<FavoriteCityViewModel> GetFavoriteCities(AppUser appUser);

        bool AddFavoriteCity(AppUser appUser, City city);

        bool RemoveFavoriteCity(AppUser appUser, City city);

        List<FavoriteCityViewModel> GetFavoriteBarsByCityName(AppUser appUser, string cityName);

        bool AddFavoriteBar(AppUser appUser, SecretBar bar);

        bool RemoveFavoriteBar(AppUser appUser, SecretBar bar);

        List<UserSharedBarViewModel> FindSharedBars(AppUser appUser);

        List<AppUser> GetAllAppUsers();

